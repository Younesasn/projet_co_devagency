<?php 
    require 'bdd.php';

    $sql = "SELECT id, prenom, nom FROM utilisateur";
    $requete = $db->query($sql); 
    $stagiaires = $requete->fetchAll();

    $html='';
    $css='';

    foreach ($stagiaires as $stagiaire) {
        $prenom = strtolower($stagiaire['prenom']);
        $nom = strtolower($stagiaire['nom']);

        $css .= '
            .stagiaire' . $stagiaire['id'] . ' {
                background-image:url(img/dossierFerme/' . $prenom . '_' . $nom . '.png)
            }
            .stagiaire' . $stagiaire['id'] . ':hover {
                background-image:url(img/dossierOuvert/' . $prenom . '_' . $nom . '_hover.png)
            }
        '; 

        $html .= '
            <div class="containerFolder stagiaire' . $stagiaire['id'] . '">
                <div>
                    <a href="profil.php?id=' . $stagiaire['id'] . '">
                        <div class="lienProfil"></div>
                    </a>
                </div>
            </div> 
        ';
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link href="style.css" rel="stylesheet">
    <style><?php echo $css; ?></style>
    <script src="app.js" defer></script>
    <script src="jquery-3.7.1.js"></script>
    <script src="jquery.waypoints.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Black+Ops+One&family=Russo+One&display=swap" rel="stylesheet">
    <title>DEV AGENCY</title>
</head>

<body id="stagiaire">

    <!-- BARRE DE NAVIGATION -->
    <header>
        <div class="logo" ><a href="index.html"><img src="img/logo.png"></a></div>
            <ul class="menu">
                <li><a href="index.html" class="liens">Accueil</a></li>
                <li><a href="#"><img src="img/Panier.png" alt="Panier"></a></li>
                <li><a href="#"><img src="img/login.png" alt="Login"></a></li>
            </ul>
    </header>


    <!-- BOUTON ICONE "RETOUR EN HAUT" EN BAS A DROITE  -->
        <div id='scroll_to_top' class='opacity'>
            <a><img src="img/scrolltop.png" alt="Retourner en haut"></a>
        </div>

        <section class="sectionProfils">

            <div class="titreSectionProfils">
                <h1> DEV AGENCY </h1> 
            </div>
            
            <div class="containerProfils">
                <?php echo $html; ?>
            </div>

        </section>
    
    <footer>
        <div class="logo2">
            <div class="logoFooter"><a href="index.html"><img src="img/logo.png"></a></div>
            
        </div>
        <div class="contact">
            <h2>Contact</h2><br>
            <p>HUMAN BOOSTER DWWM PE8<br> BURO CLUB <br> 13 rue Pierre Gilles de Gennes <br> 69007 LYON
            </p>
        </div>
        <div class="copyright">
            <p>DevAgency(c) 2023 </p>
        </div>
    </footer>
</body>
</html>
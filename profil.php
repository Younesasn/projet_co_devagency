<!-- Page Profil Type crée par Alex -->
<!-- MERCI DE NE PAS MODIFIER LE CODE QUE VOUS N'AVEZ PAS ECRIT & DE METTRE DES COMMENTAIRES !!! -->

<?php 
    require 'contenuprofil.php';

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Projet classe</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
        <script src="app.js" defer></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Black+Ops+One&family=Russo+One&display=swap" rel="stylesheet">
    </head>

    <body>


        <!-- BARRE DE NAVIGATION -->
        <header class="header-profil">
            <div class="logo" ><a href="index.html"><img src="img/logo.png"></a></div>
                <ul class="menu">
                    <li><a href="index.html"class="liens">Accueil</a></li>
                    <li><button class="nosagents"><a href="profil_liste.php">Nos agents</a></button></li>
                    <li><a href="#"><img src="img/Panier.png" alt="Panier"></a></li>
                    <li><a href="#"><img src="img/login.png" alt="Login"></a></li>
                </ul>
        </header>

        <!-- Titre de la page -->
        <h1 class="titre"><?php echo $surnom ?></h1>

        <!-- Section contenant la carte (photo, bouton, identité) et les soft-skills -->
        <section class="sectiontop">
            <!-- Div avec seulement la carte à gauche -->
            <div class="carteidd">
                <!-- Sous-section contenant l'image de profil -->
                <section class="imgbtn">

                    <div class="photo">
                        <img src="img/photo_agent/<?php echo $photo?>">
                    </div>
                    
                </section>
                <!-- Sous-section contenant l'identité -->
                <div class="identité">
                    <h3>Nom de l'agent :  
                        <div class="nom">
                            <?php echo $nom ?>
                        </div>
                    </h3>
                    <br>
                    <h3>Prénom :   
                        <div class="prenom">
                            <?php echo $prenom ?>
                        </div>
                    </h3>
                </div>
                <section class="liensprofil">
                    <a href="<?php echo $linkedin ?>" target="_blank"><img src="img/linkedin.png"></a>
                    <a href="<?php echo $github ?>" target="_blank"><img src="img/github1.png"></a>
                    <a href="cv/<?php echo $cv?>" target="_blank"><img src="img/iconeCV.png"></a>
                </section>

            </div>

            <!-- Div avec les soft-skills à droite -->
            <div class="skills">
                <h2 id="sous1">Soft (S)kills</h2>
                    <h3> <?php echo $titre_softskill1 ?> </h3>
                    <p> <?php echo $softskill1 ?> </p><br><br>
                    <h3> <?php echo $titre_softskill2 ?> </h3>
                    <p> <?php echo $softskill2 ?> </p><br><br>
                    <h3> <?php echo $titre_softskill3 ?> </h3>
                    <p> <?php echo $softskill3; ?> </p>

                <div class="callToAction">
                    <img src="img/top-secret_1_white.png">
                    <button class="btnmission">
                        <a href="<?php echo $linkedin ?>" target="_blank">Proposer une mission</a>
                    </button>
                </div>
            </div>

        
        </section>

        <!-- Section contenant la présentation -->
        <section class="presprofil">
            <div class="pres">
                <h2 id="sous2">Présentation</h2>
                <?php echo $presentation ?>
            </div>
        </section>

        <!-- Section contenant l'expérience -->
        <section class="expprofil">
            <div class="exp">
                <h2 id="sous3">Expériences</h2>
                <?php echo $experience ?>
            </div>
        </section>

        <!-- Section contenant les centres d'intérêts -->
        <section class="intprofil">
            <div class="int">
                <h2 id="sous5">Centres d'intérêt</h2>
                <?php echo $interet ?>
            </div>
        </section>

        <section class="callToActionEnd">
            <div class="callToAction2">
                <img src="img/detective_1.png">
                <button class="btnmission">
                    <a href="<?php echo $linkedin ?>" target="_blank">Proposer une mission</a>
                </button>
            </div>
        </section>

        <!-- Bouton retour à la liste des agents -->

        <section class="retourListeEnd">
            <button class="nosagents2"><a href="profil_liste.php">Retour à la liste des agents</a></button>
        </section>

        <!-- Footer de la page -->
        <footer>
            <div class="logo2">
                <div class="logoFooter"><a href="index.html"><img src="img/logo.png"></a></div>
                
            </div>
            <div class="contact">
                <h2>Contact</h2> <br>
                <p>HUMAN BOOSTER DWWM PE8<br> BURO CLUB <br> 13 rue Pierre Gilles de Gennes <br> 69007 LYON
                </p>
            </div>
            <div class="copyright">
                <p>DevAgency(c) 2023 </p>
            </div>
        </footer>
        
    </body>
</html>

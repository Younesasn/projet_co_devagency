<?php

/*

    CREATE DATABASE `projet_co_PE8` COLLATE 'utf8mb4_unicode_ci'

    CREATE TABLE `civilite` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `libelle` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `code` VARCHAR(250) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        PRIMARY KEY (`id`) USING BTREE,
        UNIQUE INDEX `code` (`code`) USING BTREE,
        UNIQUE INDEX `libelle` (`libelle`) USING BTREE
    )
    COLLATE='utf8mb4_unicode_ci' 
    ;

    INSERT INTO `civilite` (`id`, `libelle`, `code`) VALUES
        (1, 'Madame', 'Mme'),
        (2, 'Monsieur', 'Mr'),
        (3, 'Docteur', 'Dr'),
        (4, 'Professeur', 'Pr'),
        (5, 'Maître', 'Me');


    CREATE TABLE `utilisateur_type` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `libelle` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        PRIMARY KEY (`id`) USING BTREE
    )
    COLLATE='utf8mb4_unicode_ci'
    ;

    CREATE TABLE `utilisateur` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `id_civilite` INT(10) UNSIGNED NOT NULL,
        `id_type` INT(10) UNSIGNED NOT NULL,
        `nom` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `prenom` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `surnom` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `email` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci',
        `pass` BINARY(128) NULL DEFAULT '0x',
        `photo` VARCHAR(250) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `cv` VARCHAR(250) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `tel` VARCHAR(14) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
        `portfolio` VARCHAR(250) NULL COLLATE 'utf8mb4_unicode_ci',
        `linkedin` VARCHAR(250) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `github` VARCHAR(250) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `presentation` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `experience` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `couleur_signature` VARCHAR(6) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
        PRIMARY KEY (`id`) USING BTREE,
        UNIQUE INDEX `email` (`email`) USING BTREE,
        INDEX `FK_civilite` (`id_civilite`) USING BTREE,
        INDEX `FK_utilisateur_type` (`id_type`) USING BTREE,
        CONSTRAINT `FK_civilite` FOREIGN KEY (`id_civilite`) REFERENCES `civilite` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT `FK_utilisateur_type` FOREIGN KEY (`id_type`) REFERENCES `utilisateur_type` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
    )
    COLLATE='utf8mb4_unicode_ci'
    ;

    CREATE TABLE IF NOT EXISTS `softskill` (
        `id` int NOT NULL AUTO_INCREMENT,
        `id_utilisateur` int NOT NULL,
        `titre_softskill1` text COLLATE utf8mb4_unicode_ci,
        `softskill1` text COLLATE utf8mb4_unicode_ci,
        `titre_softskill2` text COLLATE utf8mb4_unicode_ci,
        `softskill2` text COLLATE utf8mb4_unicode_ci,
        `titre_softskill3` text COLLATE utf8mb4_unicode_ci,
        `softskill3` text COLLATE utf8mb4_unicode_ci,
        FOREIGN KEY (`id_utilisateur`)
    )
    COLLATE='utf8mb4_unicode_ci'
    ;

    CREATE TABLE `utilisateur_media` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `id_utilisateur` INT(10) UNSIGNED NOT NULL,
        `libelle` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        `chemin` VARCHAR(250) NOT NULL COLLATE 'utf8mb4_unicode_ci',
        PRIMARY KEY (`id`) USING BTREE,
        INDEX `FK_utilisateur` (`id_utilisateur`) USING BTREE,
        CONSTRAINT `FK_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
    )
    COLLATE='utf8mb4_unicode_ci'
    ;
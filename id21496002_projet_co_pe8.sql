-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 15 nov. 2023 à 18:05
-- Version du serveur : 10.5.20-MariaDB
-- Version de PHP : 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `id21496002_projet_co_pe8`
--

-- --------------------------------------------------------

--
-- Structure de la table `civilite`
--

CREATE TABLE `civilite` (
  `id` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `code` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `civilite`
--

INSERT INTO `civilite` (`id`, `libelle`, `code`) VALUES
(3, 'Madame', 'Mme'),
(4, 'Monsieur', 'M.');

-- --------------------------------------------------------

--
-- Structure de la table `softskill`
--

CREATE TABLE `softskill` (
  `id` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `titre_softskill1` text DEFAULT NULL,
  `softskill1` text DEFAULT NULL,
  `titre_softskill2` text DEFAULT NULL,
  `softskill2` text DEFAULT NULL,
  `titre_softskill3` text DEFAULT NULL,
  `softskill3` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `softskill`
--

INSERT INTO `softskill` (`id`, `id_utilisateur`, `titre_softskill1`, `softskill1`, `titre_softskill2`, `softskill2`, `titre_softskill3`, `softskill3`) VALUES
(1, 8, 'Motivée', 'Faire de mon mieux pour atteindre mes objectifs.', 'Capacité d\'adaptation', 'Je suis prête à apprendre et essayer de nouvelles choses. ', 'Esprit d\'équipe', 'J\'aime  travailler avec un équipe autour de projets et d\'objectifs communs.'),
(2, 9, 'Esprit d\'équipe\r\n', NULL, 'Autonomie\r\n', NULL, 'Snapchat\r\n', NULL),
(3, 10, 'Rigoureux', 'Je cherche à bien faire les choses, je me concentre sur les objectifs important et je vérifie et corrige les erreurs', 'Déterminé', 'Je fais tout pour trouver une solution à un problème', 'Patient ', 'Je prends le temps de bien comprendre les choses et de faire mes tâches même si cela prends du temps'),
(4, 12, 'Créative', 'Tout problème a sa solution', 'Esprit d’équipe', 'Je suis persuadée qu’on avance plus loin à plusieurs', 'Gestion du stress', 'Je reste focus même dans la tempête'),
(5, 13, 'Patient', 'Je suis capable de m\'adapter au rythme de travail de mes collaborateurs', 'Calme', 'Je sais gérer mes émotions et mon stress en toutes circonstances', 'Autonome', 'J\'ai la facilité de me mettre en condition de travail sans attendre à recevoir des consignes'),
(6, 19, 'Capacité d\'adaptation \r\n', 'S\'adapte aux changement ', 'Patience\r\n', 'Calme et perséverante', 'Curiosité\r\n', 'J\'aime découvrir et apprendre de nouvelles choses'),
(7, 20, 'Curiosité', 'Je pense qu\'il est essentiel d\'avoir un esprit ouvert et de toujours être à la recherche des nouvelles informations.', 'Travail en équipe', 'Fondamentalement convaincu que la coordination en groupe peut faire des merveilles ', 'Rigueur', 'Savoir suivre une procédure et être précis sont des éléments qui ont toujours été essentiels pour moi'),
(8, 21, 'Calme\r\n', NULL, 'Autonome\r\n', NULL, 'Rigoureux\r\n', NULL),
(9, 22, 'Visionnaire\r\n', NULL, 'Motivation \r\n', NULL, 'Curiosité\r\n', NULL),
(10, 23, 'Sincère\r\n', NULL, 'Curiosité\r\n', NULL, 'Souriant\r\n', NULL),
(11, 26, 'Respect\r\n', NULL, 'Ecoute active\r\n', NULL, 'Esprit d\'équipe\r\n', NULL),
(12, 27, 'Esprit d\'équipe\r\n', NULL, 'Flexible\r\n', NULL, 'Réactif\r\n', NULL),
(13, 28, 'Ecoute attentive\r\n', 'Mon point fort, être à l\'écoute de toute les demandes et des personnes.', 'Travail d\'équipe\r\n', 'Je sais m\'intégrer facilement dans une équipe et prendre le rôle qu\'on me donne à coeur.', 'Patience\r\n', 'Je sais faire preuve de patience afin de me perfectionner et finaliser ce que j\'exécute.'),
(14, 31, 'Capacité d\'adaptation\r\n', NULL, 'Ecoute active\r\n', NULL, 'Esprit d\'équipe\r\n', NULL),
(15, 32, 'Travail d\'équipe', 'En équipe, je suis un collègue investi et motivé, qui est toujours pret pour relever de nouveaux challenges. Je suis aussi polyvalent et adaptable, qui sait travailler efficacement dans différents contextes. Enfin, je suis un collègue créatif et novateur, qui est toujours à l\'affût de nouvelles solutions pour aider l\'équipe à réussir.', 'Curiosité', 'Je suis toujours à la recherche de nouvelles choses à apprendre et à découvrir. J\'aime découvrir de nouvelles cultures et de nouvelles façons de penser. Je suis aussi toujours prêt à relever de nouveaux défis, même si ça me fait sortir de ma zone de confort.', 'Aisance relationnelle', 'Je suis une personne sociable et empathique, qui sait s\'adapter à tous les milieux. J\'ai travaillé avec des enfants, des adolescents, des entreprises et des particuliers, et j\'ai toujours réussi à créer des relations positives et constructives. Je suis également capable de convaincre et de retranscrire des informations en m\'adaptant au public, en tenant compte de leurs besoins et de leurs attentes.');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_civilite` int(10) UNSIGNED NOT NULL,
  `id_type` int(10) UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `pass` binary(128) DEFAULT '0x\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `photo` varchar(250) NOT NULL,
  `cv` varchar(250) NOT NULL,
  `tel` varchar(14) DEFAULT NULL,
  `portfolio` varchar(250) DEFAULT NULL,
  `linkedin` varchar(250) NOT NULL,
  `github` varchar(250) NOT NULL,
  `presentation` text NOT NULL,
  `experience` text NOT NULL,
  `couleur_signature` varchar(6) DEFAULT NULL,
  `surnom` varchar(50) NOT NULL,
  `interet` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `id_civilite`, `id_type`, `nom`, `prenom`, `email`, `pass`, `photo`, `cv`, `tel`, `portfolio`, `linkedin`, `github`, `presentation`, `experience`, `couleur_signature`, `surnom`, `interet`) VALUES
(8, 3, 1, 'Sarr', 'Mbarka', 'mdiaw@stagiaire-humanbooster.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'mbarka_sarr.png', 'mbarka_sarr.pdf', '076720358', NULL, 'https://www.linkedin.com/in/mbarka-sarr-1283b3258/\r\n', 'https://github.com/MBARKA11/mbarka\r\n', 'Passionnée du numérique, j\'ai décidé de poursuivre mes études dans ce domaine particulièrement dans le développement web et monter ma propre boîte à l\'avenir.\r\n\r\n', 'J\'ai pas mal d\'expérience. J\'étais vendeuse saisonnière en parallèle de mes études au Sénégal avant de changer de domaine. En 2021, j\'ai décidé de continuer  mes études dans le numérique. J\'ai fait deux découvertes dans le domaine du numérique chez Simplon (la découverte des objectifs du numérique à l\'aide du cms(wordpress) et Apple Foundation: faire une application IOS avec Swift) pour avoir quelques bases nécessaires à la programmation avant d\'intégrer dans une formation développeur web et web mobile.\r\n\r\n', NULL, 'Le code honneur', 'Sport, cuisine, musique  et se cultiver(faire des recherches).'),
(9, 4, 1, 'Hamon', 'Alex', 'alex.hamon744@gmail.com', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'alex_hamon.png', 'alex_hamon.pdf', '06.43.60.07.97', NULL, 'https://www.linkedin.com/in/alex-hamon-aba9a6247\r\n', 'https://github.com/AlexHamon74\r\n', 'Je m’appelle Alex j’ai 24 ans.\nPassionné de sport et principalement de hockey sur glace, que je pratique depuis plus de 10ans.\nLes jeux vidéo sont mon échappatoire virtuel préféré, où je peux sauver le monde tout en portant des pyjamas.\nLes voyages sont mon carburant pour l\'âme, m\'offrant l\'occasion de découvrir de nouvelles cultures et de créer des souvenirs.\nJe suis très curieux, un joueur d\'équipe fiable et un professionnel sérieux.\n', 'Après le baccalauréat, j\'ai suivi ma passion pour le sport en tant qu\'étudiant, puis en tant que coach.\nCependant, je savais que ce n\'était pas ma voie à long terme.\nJ\'ai entrepris différents emplois pour réfléchir, y compris préparateur de commande en usine et mise en rayon.\nPar la suite, j\'ai travaillé en tant que technicien de piscine.\nC\'est grâce à des proches que j\'ai découvert le développement web, un domaine qui a été un véritable coup de foudre pour moi.\nJ\'ai depuis consacré mon énergie à cette nouvelle passion.\n', NULL, 'Shadow dev', 'Jeux vidéos (MMORPG), Sport (Hockey sur glace), Voyage\r\n'),
(10, 4, 1, 'Colombe', 'Arnaud', 'arnaudcolombe9@gmail.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'arnaud_colombe.png', 'arnaud_colombe.pdf', '07 67 03 24 54', NULL, 'https://www.linkedin.com/in/arnaud-colombe/', 'https://github.com/arnaud919', 'Interessé par le numérique depuis longtemps, j\'ai choisi de vouloir travailler dans ce domaine et de devenir développeur web\r\n', 'J\'ai auparavant réalisé de petits projets de réalisation de sites internets et de sites vitrines.\r\n', NULL, 'Cyber sentinel', 'Technologie\r\n'),
(12, 3, 1, 'Rolland', 'Amélie', 'amelierolland.v@gmail.com', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'amélie_rolland.png', 'amelie_rolland..pdf', '06.01.81.99.02', NULL, 'https://www.linkedin.com/in/am%C3%A9lie-rolland-h/', 'https://github.com/AmelieRolland\r\n', 'En reconversion professionnelle, j’ai une expérience forte de 10 ans \r\ndans le commerce retail. ', 'En reconversion professionnelle, j’ai une expérience forte de 10 ans dans le commerce retail. \nD’abord merchandiser, j’ai appris l’importance de <em>l’expérience client </em>, en observant, \net en anticipant leur parcours en magasin pour optimiser leur visite, et augmenter le chiffre d\'affaires.\nPuis, en tant que conseillère de vente, j’ai exercé dans différents univers naviguant de la quincaillerie de quartier\n à la maroquinerie de luxe; j’ai su m’adapter à chaque clientèle, française et internationale. \nIl était primordial de savoir écouter les besoins de chaque client pour y répondre avec précision, \nmais également d’anticiper leurs besoins (la fameuse vente additionnelle!). Mon objectif, au-delà \ndu chiffre, était bien de fidéliser notre clientèle et de la voir revenir en confiance.\n\nAu milieu de ce parcours, j’ai vécu 4 ans à Tlalnepantla, dans la banlieue de Mexico DF. \nJ’ai exercé durant cette période en tant que professeur de français FLE par le biais d’une école privée. \nJ’ai dû apprendre de mon côté des techniques d’enseignement, de pédagogie, afin de \nréussir ma nouvelle activité de la meilleure manière.\nEn tant qu’artiste, ce furent quatre années très riches; j’ai pu exposer régulièrement\n mes œuvres dans le Jardìn del Arte après la validation d’un jury. J’ai créé un groupe de musique \navec lequel nous faisions des concerts régulièrement. Nous avons aussi eu la chance de pouvoir\n jouer en live pour une chaîne de télévision locale, et de bénéficier \nd’un live/interview d’une heure pour une radio.\n\nAujourd’hui à la recherche de stabilité, je souhaite pouvoir m’épanouir dans une profession\n qui continuera à me stimuler, qui me permettra d’allier mes passions pour la créativité \net la logique. J’ai jusqu’ici une forte appétence pour le front end, et j’ai hâte de relever \nDe nouveaux défis dans les missions que vous voudrez me confier!\n', NULL, 'CSS 117', 'Artiste: dessin au feutre fin, diverses expositions au Mexique \r\net en France (www.amelierolland.com )\r\n\r\nMusicienne: guitare, piano, (divers concerts en France et au Mexique,\r\n Lives et interviews pour \r\nune émission de télé et radio mexicaine)'),
(13, 4, 1, 'Assani', 'Younes', 'younesassani@gmail.com', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'younes_assani.png', 'younes_assani.pdf', NULL, NULL, 'https://www.linkedin.com/in/younesassani', 'https://github.com/Younesasn', 'To Do', 'To Do', NULL, 'Data Dynamo', 'Football, Jeux Vidéos (FPS, Action/Aventure, Horreur...)'),
(19, 3, 1, 'Sthockmar', 'Monica', 'msthockmar@stagiaire-humanbooster.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'monica_sthockmar.png', 'monica_sthockmar.pdf', '06-59-39-07-10', NULL, 'https://www.linkedin.com/in/monica-sthockmar-a265b9264/\n', 'https://github.com/Msthockmar\r\n', 'En reconversion professionnelle j\'ai pu acquérir de nombreuses compétences \nAprès avoir fait une découverte multimédia, j\'ai décidé de m\'orienter dans le développement web pour approfondir et acquérir plus de compétences.\n', 'Après avoir obtenu mon bac en logistique j\'ai travaillé dans diverses métiers en tant garde d\'enfant pendant 2 ans qui m\'a permis d\'acquérir beaucoup de  patience puis en tant que vendeuse en boulangerie et en aide à domicile qui m\'a permit de développer mes compétences et d\'être plus autonome.\n', NULL, 'Code Cipher', 'Voyages : J\'aime découvrir de nouveaux pays et de nouvelles cultures.'),
(20, 4, 1, 'Laubscher', 'Patrick', 'patricklaubscher@hotmail.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'patrick_laubscher.png', 'patrick_laubscher.pdf', '06 78 95 51 84', NULL, 'https://www.linkedin.com/in/patrick-laubscher/\r\n', 'https://github.com/PatrickLaubscher\r\n', 'De nature curieuse et dynamique, mon parcours professionnel m’a amené à travailler pendant plusieurs années dans le commerce international ainsi que dans le développement personnel. Ces expériences ont été très enrichissantes et avec mon goût pour les défis techniques et l’informatique, j’ai décidé de me reconvertir dans le développement web. Le secteur du numérique me passionne de par sa constante expansion et les diverses possibilités qu\'il offre.\n', 'J\'ai 2 casquettes. Des années d\'expériences dans l\'import-export où j\'ai principalement travaillé dans la logistique à gérer de nombreuses informations à la fois, coordonner les informations entre les équipes, et ce très souvent en anglais. Enfin, une activité en indépendantdans le développement personnel et la naturopathie à travers laquelle j\'ai organisé beaucoup de formations et d\'ateliers. J\'ai appris plusieurs d\'outils sur la gestion du stress et des émotions, ainsi que la communication avec la PNL.', NULL, 'Code Crusader', 'Lectures en développement\npersonnel (PNL, psychologie), Gaming pc (simulations spaciales,\nRPGs, stratégies), Botanique & phytothérapie.\n'),
(21, 4, 1, 'Raffin', 'Lionel', 'raffinlionel@icloud.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'lionel_raffin.png\r\n', 'lionel_raffin.cv\r\n', '07.71.37.07.97', NULL, 'https://www.linkedin.com/in/lionelraffin85\r\n', 'https://github.com/LionelRf85\r\n', 'Je n\'ai rien contre le fait de travailler\n du moment que l\'on ne m\'oblige pas.\n', 'J\'ai une expérience dans l\'horlogerie et fabrication de bijoux.', NULL, 'The Big Front', 'La course à pied\r\n'),
(22, 4, 1, 'Baumann', 'John', 'jbaumann@stagiaire-humanbooster.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'john_baumann.png', 'john_baumann.pdf', '07.66.07.48.38', NULL, 'https://www.linkedin.com/in/john-baumann-a15528296/\r\n', 'https://github.com/iceman6900\r\n', 'Ce qui me caractérise, c\'est mon esprit visionnaire, que j\'ai tout particulièrement mis à l\'épreuve pour ma reconversion professionnelle.\nAprés une remise en question au cours de laquelle j\'ai décidé de réunir mes passsions et mes qualités j\'ai donc décidé de me former au métier de développeur Web et mobile.\n', 'J\'ai pu commmencer à utiliser divers outils de façon théorique dans le développement. N\'ayant aucune expérience dans le domaine\n je souhaiterai mettre à profit au sein d\'une entreprise la partie théorique que j\'ai pu acquérir en cours de formation.\n', NULL, 'Code Fighter', 'Science, Technologie.\r\n'),
(23, 4, 1, 'Bekka', 'Samy', 'samy.bekka@hotmail.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'samy_bekka.png', 'samy_bekka.pdf', '06 43 29 29 35', NULL, 'https://www.linkedin.com/in/samy-bekka/\r\n', 'https://github.com/SamyBEKKA\r\n', 'Ancien commercial reconverti dans le développement web, j\'ai\nacquis énormément de compétences en savoir-être et en savoir-faire\ngrâce à mes 5 ans de formation dans le commerce.\nAujourd\'hui j\'espère réussir dans les métiers du code.\n', 'Plus de 5 ans dans le commerce\n+ moins d\'un an en Developpement web\n', NULL, 'Le crakend', 'Jeux vidéo, \nFilms série et anime\nVoyager\n'),
(26, 4, 1, 'Laouadi', 'Aïmen', 'laouadiaimen@gmail.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'aimen_laouadi.png', 'aimen_laouadi.pdf', '07 67 16 94 13', NULL, 'https://www.linkedin.com/in/aimen-laouadi/\r\n', 'https://github.com/aimenlaouadi/aimenlaouadi\r\n', 'J\'ai travaillé pendant plus de\n8 ans dans l\'industrie agro-alimentaire où j\'ai pu acquérir un savoir-être et une certaine rigueur et l\'esprit d\'équipe.\n', 'J\'ai été amené à travailler pendant 8 ans expérence dans l\'industrie agro-alimentaire où j\'ai acquis un savoir être.\n', NULL, 'Mjöllnend', 'Football, jeux vidéos, Voyage\r\n'),
(27, 4, 1, 'Payet', 'Anaël', 'anaelpayetpro@gmail.com', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'anael_payet.png', 'anael_payet.pdf', '07.69.83.49.72', NULL, 'https://www.linkedin.com/in/anael-payet/\r\n', 'https://github.com/AnaelTech\r\n', 'Passionné par le développement web et en pleine reconversion professionnelle vers ce domaine dynamique. Je saurai mettre en avant les compétences que j\'ai pu acquérir durant mes expériences passées.\n', ' Une longue expérience dans le domaine de la vente.\r\n', NULL, 'HTML Phantom', 'Jeux-vidéo (FPS, auto battler), Football, Lecture \r\n'),
(28, 3, 1, 'Mira', 'Céline', 'celinemira.pro@gmail.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'celine_mira.png', 'celine_mira.pdf', '06 61 73 05 59', NULL, 'https://www.linkedin.com/in/c%C3%A9line-mira/\r\n', 'https://github.com/celinemira\r\n', 'Ancienne vendeuse dans divers domaine de la vente, aujourd\'hui, je suis créatrice de contenus photos, vidéos et lives, \net j\'anime des communautés sur le web.\nAu fil des années, j\'ai pu acquérir de l\'autonomie et apprendre en \nautodidacte des compétences dans le numérique et le design.\n', 'Durant mes expériences passées, j\'ai été vendeuse en prêt-à-porter féminin, jouets, et décorations, ce qui m\'a permis d\'avoir une écoute très attentive des besoin du client. 6 ans d\'expériences dans les réseaux sociaux où j\'ai appris davantage sur le domaine d\'internet, et d\'acquérir des compétences de graphisme.\n', NULL, 'CSS Stream', 'Réseaux sociaux, Jeux vidéo, Photographie, Musique.\r\n'),
(31, 3, 1, 'Kowal', 'Jessica', 'jkowal@stagiaire-humanbooster.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'jessica_kowal.png', 'jessica_kowal.pdf', '07.68.67.21.81', NULL, 'https://www.linkedin.com/in/jess-coco/\r\n', 'https://github.com/Jessica-creat\r\n', 'Passionnée par le développement web et mobile, je suis une professionnelle organisée, rigoureuse et dotée d\'un excellent sens du relationnel. Je suis prête à mettre à profit mes compétences techniques et mes qualités d\'équipe pour trouver un stage enrichissant en tant que développeuse web et mobile. \r\n', 'J\'ai une expérience dans la gestion administrative\r\n', NULL, 'Calamity', 'Jeux de société, séries\r\n'),
(32, 4, 1, 'Kadouri', 'Nabil', 'kadourinabil7@gmail.com\r\n', 0x3078000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 'nabil_kadouri.png', 'nabil_kadouri.pdf', '07.72.31.32.38', NULL, 'https://www.linkedin.com/in/nabil-kadouri/\r\n', 'https://github.com/NabilKADOURI\r\n', 'Je suis Nabil KADOURI, un professionnel expérimenté.\r\nJ\'ai su saisir les opportunités grâce à ma capacité d\'adaptation et mes qualités relationnelles. Un accident de travail m\'a conduit à reconsidérer ma carrière. Aujourd\'hui, je me lance dans une reconversion en tant que développeur web. Passionné par ce domaine, je souhaite contribuer à l\'innovation technologique et à la création d\'un monde plus accessible. Je suis prêt pour une nouvelle aventure !', 'Organisateur d\'événements et animateur professionnel, je possède une solide expérience en gestion de projets, communication, collaboration et gestion du temps. Je suis également un excellent communicant et j\'ai un fort sens du service client.\r\n\r\nJ\'ai organisé des événements festifs et sportifs pour tous les publics, notamment un festival d\'été de plusieurs jours avec des concerts, des spectacles et des animations pour tous les âges, un concours de talents ouvert à tous, un tournoi de football caritatif au profit d\'une association locale et un carnaval annuel qui attire plus de 500 visiteurs. J\'ai également dirigé une équipe d\'animation de 10 personnes et géré l\'ensemble des aspects administratifs et financiers d\'un centre de loisirs accueillant 50 enfants par jour.\r\n\r\nEn tant que télévendeur en BtoB, j\'ai géré un portefeuille client de plus de 100 entreprises et mis en place des campagnes publicitaires . En tant que conseiller financier, j\'ai commercialisé des solutions financières à des particuliers, notamment  des prêts et des assurances.', NULL, 'Firecode', 'Je suis un sportif dans l\'âme, passionné par le football et les activités de plein air. Je suis entraîneur de football depuis 10 ans catégorie U17 ans, et je suis également un féru de nouvelles technologies comme la réalité virtuel et augmentée, J\'aime le gaming (notamment les MOBA et MMO RPG comme League of légend , Warcraft ou NORTHGARD. J\'aime profiter de mes temps libres pour faire du vélo ou de la natation.');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_media`
--

CREATE TABLE `utilisateur_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_utilisateur` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `chemin` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_type`
--

CREATE TABLE `utilisateur_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `utilisateur_type`
--

INSERT INTO `utilisateur_type` (`id`, `libelle`) VALUES
(1, 'Stagiaire');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `civilite`
--
ALTER TABLE `civilite`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `code` (`code`) USING BTREE,
  ADD UNIQUE KEY `libelle` (`libelle`) USING BTREE;

--
-- Index pour la table `softskill`
--
ALTER TABLE `softskill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_utilisateur` (`id_utilisateur`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `email` (`email`) USING BTREE,
  ADD UNIQUE KEY `tel` (`tel`) USING BTREE,
  ADD KEY `FK_civilite` (`id_civilite`) USING BTREE,
  ADD KEY `FK_utilisateur_type` (`id_type`) USING BTREE;

--
-- Index pour la table `utilisateur_media`
--
ALTER TABLE `utilisateur_media`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `FK_utilisateur` (`id_utilisateur`) USING BTREE;

--
-- Index pour la table `utilisateur_type`
--
ALTER TABLE `utilisateur_type`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `civilite`
--
ALTER TABLE `civilite`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `softskill`
--
ALTER TABLE `softskill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `utilisateur_media`
--
ALTER TABLE `utilisateur_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateur_type`
--
ALTER TABLE `utilisateur_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `FK_civilite` FOREIGN KEY (`id_civilite`) REFERENCES `civilite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_utilisateur_type` FOREIGN KEY (`id_type`) REFERENCES `utilisateur_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `utilisateur_media`
--
ALTER TABLE `utilisateur_media`
  ADD CONSTRAINT `FK_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

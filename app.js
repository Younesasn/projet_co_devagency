const header = document.querySelector('header');

window.addEventListener('scroll', () => {
    if(window.scrollY > 100) {
        header.classList.add('scroll');
    }
    else{
        header.classList.remove('scroll');
    }
});

// <!-- SCRIPT COUNTER INFOGRAPHIE: -->

    $(document).ready(() => {
    $('.chiffre').counterUp({
        time: 1000
    });
});


// <!-- SCRIPT SCROLL DOWN VIDEO: -->
	$(document).ready(function() {
		$('.js-scrollTo').on('click', function() { // Au clic sur un élément
			var page = $(this).attr('href'); // Page cible
			var speed = 950; // Durée de l'animation (en ms)
			$('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
			return false;
		});
	});


// <!-- SCRIPT RETOUR EN HAUT: -->
    $(document).ready(function() {
        function scroll_to_top(div) {
            $(div).click(function() {
                var speed = 1200;
                $('html,body').animate({scrollTop: 0}, speed);
            });
            $(window).scroll(function(){
                if($(window).scrollTop()<500){
                    $(div).fadeOut();
                } else{
                    $(div).fadeIn();
                }
            });
        }
        scroll_to_top("#scroll_to_top");
    }); 

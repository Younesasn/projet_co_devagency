<?php 
    require 'bdd.php';

    $id = $_GET['id'];

    $sql =  "SELECT u.*, s.*
            FROM utilisateur u
            INNER JOIN softskill s
            ON s.id_utilisateur = u.id 
            WHERE u.id = :id";
/*          INNER JOIN utilisateur_media m
            ON m.id_utilisateur = u.id
            WHERE u.id = :id
 */            

    $requete = $db->prepare($sql);
    $requete->bindParam(':id', $id, PDO::PARAM_INT);
    $requete->execute();
    
    $stagiaire = $requete->fetch(PDO::FETCH_ASSOC);
    
    if ($stagiaire) {

        $surnom = $stagiaire['surnom'];
        $nom = $stagiaire['nom'];
        $prenom = $stagiaire['prenom'];
        $photo = $stagiaire['photo'];
        $email = $stagiaire['email'];
        $linkedin = $stagiaire['linkedin'];
        $github = $stagiaire['github'];
        $cv = $stagiaire['cv'];
        $experience = $stagiaire['experience'];
        $presentation = $stagiaire['presentation'];
        $titre_softskill1 = $stagiaire['titre_softskill1'];
        $softskill1 = $stagiaire['softskill1'];
        $titre_softskill2 = $stagiaire['titre_softskill2'];
        $softskill2 = $stagiaire['softskill2'];
        $titre_softskill3 = $stagiaire['titre_softskill3'];
        $softskill3 = $stagiaire['softskill3'];
        $interet = $stagiaire['interet'];
    } 
    
    else {
        // Cas où l'ID fourni ne correspond à aucun agent-stagiaire
        echo "Agent-stagiaire non trouvé.";
    }

?>